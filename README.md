![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

Tool which automatically publishes all available BuildStream documentation
using Gitlab's CI.

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: alpine:3.8

pages:
  stage: deploy
  script:
    - apk add --update git
    - chmod a+x generate_pages.sh
    - ./generate_pages.sh
  artifacts:
    paths:
    - public
  only:
  - master

```

## Generate pages script

Most of the work is done using a simple shell script which can be found
at [`.generate_pages.sh`](generate_pages.sh):

```
#!/bin/sh

# Small script which fetches all the tags from the BuildStream repository,
# downloads all the documentation artifacts and then creates a list of the
# links in the index.html.

# This line is always present in the index.html so that we can automatically
# create the list of tags.
changed_line="<!-- Change this line with the list of available versions. -->"

# Fetch all the tags and parse them.
tags=$(git ls-remote --tags https://gitlab.com/BuildStream/buildstream | awk -F[/\^] '{print $3}' | uniq | grep [0-9]\.[0-9]\.[0-9] | tac)
# Manualy add master there
tags=$(echo -e "master\n$tags")

# Create the list of the HTML href's we'll insert into index.html
HTML_tags=""

for tag in $tags ; do
  # Everything needs to be in public/${version}/
  wget https://gitlab.com/BuildStream/buildstream/-/jobs/artifacts/$tag/download?job=docs -O $tag.zip 1>/dev/null
  mkdir public/$tag
  unzip $tag.zip -d public/$tag
  mv public/$tag/public/* public/$tag
  HTML_tags=$(echo "${HTML_tags}\n<li class=\"toctree-l1\"><a class=\"reference internal\" href=\"${tag}/index.html\">${tag}</a></li>")
done
# Substitute the target line, creating the table of entries automatically.
HTML_tags=$(echo $HTML_tags | tail -c +3)
sed -i "s#${changed_line}#${HTML_tags}#g" public/index.html
```

[ci]: https://about.gitlab.com/gitlab-ci/
